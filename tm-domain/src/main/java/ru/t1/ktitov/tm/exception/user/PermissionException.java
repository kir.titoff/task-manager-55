package ru.t1.ktitov.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Insufficient rights.");
    }

}
