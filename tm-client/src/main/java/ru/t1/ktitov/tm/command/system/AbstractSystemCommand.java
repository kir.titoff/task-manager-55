package ru.t1.ktitov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ktitov.tm.api.service.ICommandService;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

}
