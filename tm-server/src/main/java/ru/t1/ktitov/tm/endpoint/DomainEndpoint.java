package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktitov.tm.api.service.IDomainService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.dto.request.data.*;
import ru.t1.ktitov.tm.dto.response.data.*;
import ru.t1.ktitov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxBLoadResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonJaxBLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataJsonJaxBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxBSaveResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonJaxBSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataJsonJaxBSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxBLoadResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlJaxBLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataXmlJaxBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxBSaveResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlJaxBSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataXmlJaxBSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}
